import java.util.*
import kotlin.random.Random

fun main(args: Array<String>) {
    /// 1
    val myArray = arrayOfNulls<Int>(20)
    for (i in 0 until myArray.size) {
        myArray[i] = Random.nextInt(10) + 1
    }
    println(Arrays.toString(myArray))
    /// 2
    val list = mutableListOf<Int>()
    while (10 !in list) {
        list.add(Random.nextInt(10) + 1)
    }
    println(list)
    /// 3
    val occurrence = mutableMapOf<Int, Int>()
    for (e in list) {
        occurrence[e] = occurrence[e]?.plus(1) ?: 1
    }
    println(occurrence)
    // HW
    val mostFamousNames = arrayOf(
                "Marie",
                "Jiří",
                "Jan",
                "Jana",
                "Petr",
                "Petr",
                "Pavel",
                "Jaroslav",
                "Martin",
                "Tomáš",
                "Miroslav",
                "Eva",
                "František",
                "Anna",
                "Hana",
                "Josef",
                "Štěpán",
                "Štepán",
                "Věra",
                "Václav"
        )
        val occurrence2 = mutableMapOf<Char, Int>()
        for (name in mostFamousNames) {
            for (letter in name) {
                occurrence2[letter] = occurrence2[letter]?.plus(1) ?: 1
            }
        }
        println(occurrence2)
}
